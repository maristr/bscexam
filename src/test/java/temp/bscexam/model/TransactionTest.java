package temp.bscexam.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TransactionTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCantCreateDueUnsupportedCurrencyMarker() throws Exception {
        Transaction.fromCurrencyAndAmount("CCC", "100");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCantCreateDueZeroAmount() throws Exception {
        Transaction.fromCurrencyAndAmount("USD", "0");
    }

    @Test(expected = NullPointerException.class)
    public void testCantCreateDueNullAmount() throws Exception {
        Transaction.fromCurrencyAndAmount("USD", null);
    }

    @Test(expected = NullPointerException.class)
    public void testCantCreateDueNullCurrency() throws Exception {
        Transaction.fromCurrencyAndAmount(null, "0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCantCreateDueMissingArgument() throws Exception {
        Transaction.fromString("USD ");
    }

    @Test(expected = NullPointerException.class)
    public void testCantCreateFromNullRecord() throws Exception {
        Transaction.fromString(null);
    }

    @Test
    public void testCanCreateFor100USDDeposit() throws Exception {
        Transaction tx = Transaction.fromString("USD 100");
        assertNotNull(tx);
        assertTrue(tx.getCurrencyMarker().equals("USD"));
        assertTrue(tx.getAmount().compareTo(new BigDecimal("100")) == 0);
        assertTrue(tx.getType().equals(TransactionType.DEPOSIT));
    }

    @Test
    public void testCanCreateFor10EURWithdrawal() throws Exception {
        Transaction tx = Transaction.fromString("EUR -10");
        assertNotNull(tx);
        assertTrue(tx.getCurrencyMarker().equals("EUR"));
        assertTrue(tx.getAmount().compareTo(BigDecimal.TEN.negate()) == 0);
        assertTrue(tx.getType().equals(TransactionType.WITHDRAWAL));
    }
}