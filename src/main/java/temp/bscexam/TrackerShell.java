package temp.bscexam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.shell.core.JLineShellComponent;
import temp.bscexam.springshell.configuration.ShellExitHandler;


/**
 * Shell bootstrap.
 * <p/>
 * Created by marmax on 18.3.2015.
 */
@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackageClasses = {TrackerShell.class})
public class TrackerShell implements CommandLineRunner {

    @Autowired
    private JLineShellComponent shell;

    @Autowired
    private ShellExitHandler exitHandler;

    public static void main(String[] args) {
        new SpringApplicationBuilder(TrackerShell.class).showBanner(false).run(args);
    }

    @Override
    public void run(String... arg) throws Exception {
        shell.addShellStatusListener(exitHandler);
        shell.start();
        shell.setDevelopmentMode(true);
        // shell.executeCommand("import tracker.txt");
        shell.waitForComplete();
    }
}
