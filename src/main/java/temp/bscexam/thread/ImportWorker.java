package temp.bscexam.thread;

import temp.bscexam.model.Transaction;

import java.io.*;
import java.util.concurrent.BlockingQueue;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by marmax on 16.3.2015.
 */
public class ImportWorker implements Runnable {

    private final File importFile;
    private final BlockingQueue<Transaction> sharedQueue;
    private final PrintStream out;

    public ImportWorker(File importFile, BlockingQueue<Transaction> sharedQueue, PrintStream out) {
        checkNotNull(importFile);
        checkNotNull(sharedQueue);
        checkNotNull(out);

        this.importFile = importFile;
        this.sharedQueue = sharedQueue;
        this.out = out;
    }

    @Override
    public void run() {
        if (!this.importFile.exists()) {
            out.println(String.format("No import data found, expecting: %s", importFile.getAbsolutePath()));
            return;
        }
        importFile(importFile);
    }

    // TODO: use !Thread.currentThread().isInterrupted() to enable cancellation
    private void importFile(File file) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            int i = 0;
            String line = br.readLine();
            while (line != null) {
                i++;
                if (line.trim().length() > 0 && !line.trim().startsWith("#")) {
                    try {
                        Transaction tx = Transaction.fromString(line);
                        sharedQueue.put(tx);
                    } catch (IllegalArgumentException iae) {
                        out.println(String.format("Invalid record at line %s, underlying error is: %s", i, iae.getMessage()));
                    } catch (InterruptedException ie) {
                        out.println(String.format("A problem has occured processing line %s, underlying error is: %s", i, ie.getMessage()));
                    }
                }
                line = br.readLine();
            }
            br.close();
        } catch (IOException ioe) {
            out.println(String.format("A problem has occured while importing data, underlying error is: %s", ioe.getMessage()));
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException swallowed) {
                }
            }
        }
    }
}
