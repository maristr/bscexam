package temp.bscexam.thread;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by marmax on 17.3.2015.
 */
public class ThreadServiceFactory {

    public static ThreadPoolExecutor create(final String threadNamePrefix, final boolean daemon) {
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(1, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = Executors.defaultThreadFactory().newThread(runnable);
                thread.setName(threadNamePrefix + " " + runnable.hashCode());
                thread.setDaemon(daemon);
                return thread;
            }
        });
    }
}
