package temp.bscexam.utils;

import org.fusesource.jansi.Ansi;

/**
 * wrappes text with Jansi escape characters for given color
 * <p/>
 * Created by marmax on 19.3.2015.
 */
public class JansiColors {

    // BLACK, RED, GREEN, CYAN, WHITE, DEFAULT;

    public static String wrappedAsRed(String text) {
        return Ansi.ansi().fg(Ansi.Color.RED).a(text).toString();
    }

    public static String wrappedAsYellow(String text) {
        return Ansi.ansi().fg(Ansi.Color.YELLOW).a(text).toString();
    }

    public static String wrappedAsMagenta(String text) {
        return Ansi.ansi().fg(Ansi.Color.MAGENTA).a(text).toString();
    }

    public static String wrappedAsBlue(String text) {
        return Ansi.ansi().fg(Ansi.Color.BLUE).a(text).toString();
    }

    public static String wrappedAsCyan(String text) {
        return Ansi.ansi().fg(Ansi.Color.CYAN).a(text).toString();
    }

    public static String wrappedAsGreen(String text) {
        return Ansi.ansi().fg(Ansi.Color.GREEN).a(text).toString();
    }

}
