package temp.bscexam.model;

/**
 * Created by marmax on 19.3.2015.
 */
public enum Toggle {

    ON(true),
    OFF(false);
    private final boolean state;

    private Toggle(boolean state) {
        this.state = state;
    }

    public static Toggle fromString(String toggleStr) {
        if ("ON".equalsIgnoreCase(toggleStr)) {
            return Toggle.ON;
        } else if ("OFF".equalsIgnoreCase(toggleStr)) {
            return Toggle.OFF;
        }
        return null;
    }

    public static Toggle fromBoolean(boolean toggleBool) {
        return (toggleBool) ? ON : OFF;
    }

    public boolean asBoolean() {
        return this.state;
    }
}
