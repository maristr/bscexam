package temp.bscexam.model;

import java.math.BigDecimal;

/**
 * Created by marmax on 16. 3. 2015.
 */
public class CurrencyAccount {

    private final String currencyMarker;
    private volatile BigDecimal balance = BigDecimal.ZERO;


    public CurrencyAccount(String currencyMarker) {
        this.currencyMarker = currencyMarker;
    }

    public synchronized void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    public synchronized void withdraw(BigDecimal amount) {
        balance = balance.add(amount);
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public synchronized void setBalance(BigDecimal newBalance) {
        balance = newBalance;
    }

    public String getCurrencyMarker() {
        return currencyMarker;
    }

}
