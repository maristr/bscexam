package temp.bscexam.model;

/**
 * Created by marmax on 16. 3. 2015.
 */
public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL;
}
