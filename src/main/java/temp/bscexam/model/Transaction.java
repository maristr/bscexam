package temp.bscexam.model;

import java.math.BigDecimal;
import java.util.Currency;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by marmax on 16. 3. 2015.
 */
public class Transaction {

    private final TransactionType type;
    private final String currencyMarker;
    private final BigDecimal amount;

    public Transaction(TransactionType type, String currencyMarker, BigDecimal amount) {
        checkNotNull(type);
        checkNotNull(currencyMarker);
        checkNotNull(amount);

        this.type = type;
        this.currencyMarker = currencyMarker;
        this.amount = amount;
    }

    public static Transaction fromCurrencyAndAmount(String currencyMarker, String amount) {
        checkNotNull(currencyMarker);
        checkNotNull(amount);

        try {
            Currency.getInstance(currencyMarker.toUpperCase());
        } catch (IllegalArgumentException iae) {
            throw new IllegalArgumentException("Unknown/Unsupported currency marker: " + currencyMarker, iae);
        }

        BigDecimal bdAmount = new BigDecimal(amount);
        if (bdAmount.compareTo(BigDecimal.ZERO) == 0) {
            throw new IllegalArgumentException("Tx with zero amount cannot be accepted!");
        }

        TransactionType txType = (bdAmount.compareTo(BigDecimal.ZERO) == 1) ? TransactionType.DEPOSIT : TransactionType.WITHDRAWAL;

        return new Transaction(txType, currencyMarker.toUpperCase(), bdAmount);
    }

    public static Transaction fromString(String record) {
        checkNotNull(record);

        final String[] tokens = record.split("\\s+");

        if (tokens.length < 2) {
            throw new IllegalArgumentException("Wrong TX record: " + record);
        }

        return fromCurrencyAndAmount(tokens[0], tokens[1]);
    }

    public TransactionType getType() {
        return type;
    }

    public String getCurrencyMarker() {
        return currencyMarker;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transaction{"
                + "type=" + type
                + ", currencyMarker='" + currencyMarker + '\''
                + ", amount=" + amount
                + '}';
    }
}
