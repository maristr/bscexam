package temp.bscexam.tx.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import temp.bscexam.TrackerShell;
import temp.bscexam.model.CurrencyAccount;
import temp.bscexam.model.Transaction;
import temp.bscexam.tx.AccountManager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by marmax on 16. 3. 2015.
 */
@Component
public class AccountManagerImpl implements AccountManager {

    private final Logger logger = LogManager.getLogger(TrackerShell.class);

    private ConcurrentMap<String, CurrencyAccount> currencyAccounts = new ConcurrentHashMap<>(16, 0.9f, 1);

    @Override
    public void process(Transaction transaction) {
        final String marker = transaction.getCurrencyMarker();

        CurrencyAccount currencyAccount = currencyAccounts.get(marker);
        if (currencyAccount == null) {
            currencyAccount = new CurrencyAccount(marker);
            currencyAccounts.putIfAbsent(marker, currencyAccount);
            currencyAccount = currencyAccounts.get(marker);
        }

        switch (transaction.getType()) {
            case DEPOSIT:
                currencyAccount.deposit(transaction.getAmount());
                break;
            case WITHDRAWAL:
                currencyAccount.withdraw(transaction.getAmount());
                break;
        }
    }

    @Override
    public void reportBalances() {
        StringBuilder sb = new StringBuilder();

        if (currencyAccounts.entrySet().size() == 0) {
            sb.append("+++ no accounts yet! +++");
        } else {
            sb.append("\n");
            sb.append(">----------------------------------------------------\n");
            for (Map.Entry<String, CurrencyAccount> e : currencyAccounts.entrySet()) {
                CurrencyAccount val = e.getValue();
                sb.append(val.getCurrencyMarker() + " " + val.getBalance() + "\n");
            }
            sb.append("----------------------------------------------------<\n");
        }
        logger.info(sb.toString());
    }
}
