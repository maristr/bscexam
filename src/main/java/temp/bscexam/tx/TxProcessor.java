package temp.bscexam.tx;

import temp.bscexam.model.Transaction;
import temp.bscexam.queue.Consumer;

import java.util.concurrent.BlockingQueue;

/**
 * Created by marmax on 16. 3. 2015.
 */
public class TxProcessor extends Consumer<Transaction> {

    private final AccountManager accountManager;

    public TxProcessor(AccountManager accountManager, BlockingQueue<Transaction> sharedQueue) {
        super(sharedQueue);
        this.accountManager = accountManager;
    }

    @Override
    protected void consume(Transaction transaction) {
        accountManager.process(transaction);
    }
}
