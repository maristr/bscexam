package temp.bscexam.tx;

import temp.bscexam.model.Transaction;

/**
 * Created by marmax on 16.3.2015.
 */
public interface AccountManager {
    void process(Transaction transaction);

    void reportBalances();
}
