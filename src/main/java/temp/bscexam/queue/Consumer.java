package temp.bscexam.queue;

import java.util.concurrent.BlockingQueue;


/**
 * Created by marmax on 16. 3. 2015.
 */
public abstract class Consumer<E> implements Runnable {

    private final BlockingQueue<E> sharedQueue;

    public Consumer(BlockingQueue<E> sharedQueue) {
        this.sharedQueue = sharedQueue;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                consume(sharedQueue.take());
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    protected abstract void consume(E queueItem);
}
