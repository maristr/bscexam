package temp.bscexam.springshell;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultBannerProvider;
import org.springframework.shell.support.util.FileUtils;
import org.springframework.stereotype.Component;

/**
 * Created by marmax on 18.3.2015.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TrackerBannerProvider extends DefaultBannerProvider {

    public String getBanner() {
        StringBuilder builder = new StringBuilder();
        builder.append(FileUtils.readBanner(TrackerBannerProvider.class, "banner.txt"));
        return builder.toString();
    }

    public String getVersion() {
        return "1.0";
    }

    public String getWelcomeMessage() {
        return "Welcome to Payment Tracker CLI, version " + getVersion() + ";\n\nFor command and param completion press TAB, for assistance type 'hint'.";
    }

    @Override
    public String getProviderName() {
        return "Payment Tracker Banner";
    }
}