package temp.bscexam.springshell;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultPromptProvider;
import org.springframework.stereotype.Component;

/**
 * Created by marmax on 18.3.2015.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TrackerPromptProvider extends DefaultPromptProvider {

    @Override
    public String getPrompt() {
        return "tracker>";
    }


    @Override
    public String getProviderName() {
        return "Tracker prompt provider";
    }

}
