package temp.bscexam.springshell.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import temp.bscexam.model.Transaction;
import temp.bscexam.tx.AccountManager;
import temp.bscexam.tx.TxProcessor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by marmax on 20.3.2015.
 */
@Configuration
public class AppConfiguration {

    private final static int SCHEDULER_THREAD_POOL_SIZE = 10;
    private final static int THREAD_POOL_SIZE = 10;
    @Autowired
    private AccountManager accountManager;

    @Bean(destroyMethod = "shutdown")
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(SCHEDULER_THREAD_POOL_SIZE);
        scheduler.setThreadNamePrefix("schedtask-");
        scheduler.setAwaitTerminationSeconds(60);
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        return scheduler;
    }

    @Bean(destroyMethod = "shutdown")
    public ThreadPoolTaskExecutor taskExecutor() {
        final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(THREAD_POOL_SIZE);
        taskExecutor.setThreadNamePrefix("task-");
        return taskExecutor;
    }

    @Bean
    public ThreadPoolExecutorFactoryBean getThreadPoolExecutorFactoryBean() {
        return new ThreadPoolExecutorFactoryBean();
    }


    @Bean(name = "sharedTxQueue")
    public BlockingQueue<Transaction> createSharedTxQueue() {
        return new LinkedBlockingQueue<>();
    }

    @Bean
    public TxProcessor txProcessor() {
        TxProcessor txProcessor = new TxProcessor(accountManager, createSharedTxQueue());
        taskExecutor().execute(txProcessor);
        return txProcessor;
    }
}


