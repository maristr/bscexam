package temp.bscexam.springshell.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.shell.event.ShellStatus;
import org.springframework.shell.event.ShellStatusListener;
import org.springframework.stereotype.Component;

/**
 * Created by marmax on 20.3.2015.
 */
@Component
public class ShellExitHandler implements ShellStatusListener {

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Override
    public void onShellStatusChange(ShellStatus oldStatus, ShellStatus newStatus) {
        // System.out.println(String.format("shellStatus change: [%S] -> [%s]",oldStatus.getStatus().name(), newStatus.getStatus().name()));
        if (newStatus.getStatus().equals(ShellStatus.Status.SHUTTING_DOWN)) {
            System.out.println("+++ SHUTTING DOWN +++");
            taskScheduler.shutdown();
            taskExecutor.shutdown();
        }
    }
}

