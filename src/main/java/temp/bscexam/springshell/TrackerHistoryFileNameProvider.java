package temp.bscexam.springshell;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultHistoryFileNameProvider;
import org.springframework.stereotype.Component;

/**
 * Created by marmax on 18.3.2015.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TrackerHistoryFileNameProvider extends DefaultHistoryFileNameProvider {

    public String getHistoryFileName() {
        return "tracker.history.log";
    }

    @Override
    public String getProviderName() {
        return "Tracker history file name provider";
    }

}
