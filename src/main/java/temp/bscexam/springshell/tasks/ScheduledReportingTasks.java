package temp.bscexam.springshell.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import temp.bscexam.model.Toggle;
import temp.bscexam.tx.AccountManager;

import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Created by marmax on 19.3.2015.
 */
@Component
public class ScheduledReportingTasks {

    private AtomicBoolean reportingEnabled = new AtomicBoolean(true);

    @Autowired
    private AccountManager accountManager;

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void reportAccountBalances() {
        if (reportingEnabled.get()) {
            accountManager.reportBalances();
        }
    }

    /**
     * Toggle on/off reporting.
     *
     * @param newValue the new value
     * @return the previous value
     */
    public Toggle toggleReporting(Toggle newValue) {
        return Toggle.fromBoolean(reportingEnabled.getAndSet(newValue.asBoolean()));
    }

    public Toggle getReportingState() {
        return Toggle.fromBoolean(reportingEnabled.get());
    }
}