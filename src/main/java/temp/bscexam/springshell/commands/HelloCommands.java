package temp.bscexam.springshell.commands;

import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.stereotype.Component;

import static temp.bscexam.utils.JansiColors.wrappedAsYellow;

/**
 * Created by marmax on 18.3.2015.
 */
@Component
public class HelloCommands implements CommandMarker {

    public final static String HELLO_MESSAGE = "-- hello, tracker --";

    @CliCommand(value = "hello", help = "Prints hello message")
    public String hello() {
        return wrappedAsYellow(HELLO_MESSAGE);
    }
}

