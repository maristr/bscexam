package temp.bscexam.springshell.commands;

import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import temp.bscexam.model.Transaction;

import javax.annotation.Resource;
import java.util.concurrent.BlockingQueue;

import static temp.bscexam.utils.JansiColors.*;

/**
 * Created by marmax on 18.3.2015.
 */
@Component
public class TxCommands implements CommandMarker {

    @Resource(name = "sharedTxQueue")
    private BlockingQueue<Transaction> sharedTxQueue;

    @CliCommand(value = "import", help = "reports about actual accounts")
    public String importFile(
            @CliOption(key = {"buffer"}, mandatory = true, optionContext = "commandArgument",
                    help = "name of the file to import ")
            String buffer) {
        return wrappedAsYellow(String.format("import of %s done!", buffer));
    }

    @CliCommand(value = "put", help = "process new transaction")
    public String putTx(
            @CliOption(key = {""}, mandatory = true, optionContext = "txArguments",
                    help = "currencyMarker and amount")
            Transaction tx) {

        StringBuilder sb = new StringBuilder();
        try {
            sharedTxQueue.put(tx);
            sb.append(wrappedAsBlue(String.format("processed [%s]", tx.toString())));
        } catch (InterruptedException e) {
            sb.append(wrappedAsRed("Can't process transaction, got interrupted!"));
        }
        return sb.toString();
    }
}

