package temp.bscexam.springshell.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.stereotype.Component;
import temp.bscexam.model.Toggle;
import temp.bscexam.springshell.tasks.ScheduledReportingTasks;

import static temp.bscexam.utils.JansiColors.wrappedAsBlue;
import static temp.bscexam.utils.JansiColors.wrappedAsRed;

/**
 * Created by marmax on 18.3.2015.
 */
@Component
public class ReportingCommands implements CommandMarker {

    @Autowired
    private ScheduledReportingTasks scheduledTasks;

    @CliCommand(value = "report now", help = "reports about actual accounts")
    public void report() {
        scheduledTasks.reportAccountBalances();
    }

    @CliCommand(value = "report status", help = "toggles reporting on")
    public String getReportingState() {
        return wrappedAsBlue(String.format("Reporting is [%s]", scheduledTasks.getReportingState().name()));
    }

    @CliCommand(value = "report on", help = "toggles reporting on")
    public String toggleReportOn() {
        return toggleReporting(Toggle.ON);
    }

    @CliCommand(value = "report off", help = "toggles reporting off")
    public String toggleReportOff() {
        return toggleReporting(Toggle.OFF);
    }

    private String toggleReporting(Toggle requiredState) {
        final Toggle oldState = scheduledTasks.toggleReporting(requiredState);
        if (oldState.equals(requiredState)) {
            return wrappedAsBlue(String.format("Reporting still [%s]", oldState.name()));
        }
        return wrappedAsRed(String.format("Reporting [%s] -> [%s]", oldState.name(), requiredState.name()));
    }
}

