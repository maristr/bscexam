package temp.bscexam.springshell.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.Completion;
import org.springframework.shell.core.Converter;
import org.springframework.shell.core.JLineShellComponent;
import org.springframework.shell.core.MethodTarget;
import org.springframework.stereotype.Component;
import temp.bscexam.model.Transaction;

import java.util.List;

/**
 * Created by marmax on 20.3.2015.
 */
@Component
public class TransactionConverter implements Converter<Transaction> {

    @Autowired
    private JLineShellComponent shell;

    @Override
    public boolean supports(final Class<?> requiredType, String optionContext) {
        // return String.class.isAssignableFrom(requiredType) &&
        return optionContext.contains("txArguments");
    }

    @Override
    public Transaction convertFromText(String value, Class<?> targetType, String optionContext) {
        return Transaction.fromString(value);
    }

    @Override
    public boolean getAllPossibleValues(List<Completion> completions, Class<?> targetType, String existingData, String optionContext, MethodTarget target) {
        return true;
    }
}