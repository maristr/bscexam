This project represents a simple transaction tracking application, hereinafter simply Tracker, which holds a balance on per currency basis.

## Build & Run

Project is built using Gradle 2.3. As [Gradle Wrapper](https://gradle.org/docs/current/userguide/gradle_wrapper.html) is configured, it should be enough to:

clone the project:

    $ git clone git@bitbucket.org:maristr/bscexam.git
    $ cd bscexam

and build using gradle:

    $ gradlew clean build distZip

As those tasks are configured as the default ones, you can simply issue:

    $ gradlew

The build will produce ready-to-run distribution package:

    target\distributions\bsc-tracker.zip

To use it, unzip it and run generated bat/sh script:

    $ unzip target/distributions/bsc-tracker.zip

Start the shell

    $ bsc-tracker\bin\bsc-tracker.bat

You might also want to run in directly, without using the distribution zip. In such a case, issue following commands:

    $ gradlew clean build installDist
    $ target\bsc-tracker\bin\bsc-tracker.bat

## Startup & Initialization

The tracker considers the following:
* initial balance of any per currency account is ZERO
* it tries to load Tracker Init file (see below for details)
* when Tracker is initialized it reads the console input for any commands until exit command gets issued

### Tracker Init File
The Tracker upon a start detects the existence of a file named tracker-init.txt
If such file exits, Tracker tries to load the transactions from it, line by line, having a format of:
<CURRENCY> <AMOUNT>

*Note: Any invalid record/line is skipped, the processing continues*

## Balance reporting
The Tracker reports actual balance of registered currencies (those with any transaction) once per a minute (the reporting starts with initial delay of one minute)

## Console Commands
The console is capable to process following commands:

#### PUT
    PUT <CURRENCY> <AMOUNT>
    e.g. 'PUT CZK 1000'

#### EXIT
You can terminate the application by issuing 'exit' command at the prompt.

## TODO/CAVEATS
* logging - unfortunately, the reporting may interfere with prompt line


